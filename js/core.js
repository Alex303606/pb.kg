$(document).ready(function() {
	//Скролл до элемента
	$(".order-now").on("click", function (event) {
  		if($(window).width() <= 1010) {
  			$('#menu').slideUp();  			
  		}
	    event.preventDefault();

	    var id  = $(this).attr('href'),

	    top = $(id).offset().top;
	      
	    $('body,html').animate({scrollTop: top}, 1000);
	});  

	$('.slider').slick({
		dots: true,
		appendDots:$('.dots-container')
	});

	$('.popular-slider').slick({
	  infinite: true,
	  speed: 300,
	  autoplay: true,
	  slidesToShow: 8,
	  slidesToScroll: 4,
	  responsive: [
	    {
	      breakpoint: 1170,
	      settings: {
	        slidesToShow: 6,
	        slidesToScroll: 3	               
	      }
	    },
	    {
	      breakpoint: 1023,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 4
	      }
	    },
	    {
	      breakpoint: 767,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    }	    
	  ]
	});	

	//Калькулятор
	var start = [-50,0,460,660];

	$('#rangeFirst').ionRangeSlider({
	    grid: true,
	    min: 12,
	    max: 112,
	    from: 12,
	    step: 4,
	    prettify_enabled: true
	});		

	$('#1520,#1515').on('change',function () {			
		start[0] = -165;		
	});

	$('#2030,#2020,#2115').on('change',function () {		
		start[0] = -50;		
	});	

	$('#rangeSecond').ionRangeSlider({
	    grid: true,
	    min: 20,
	    max: 50,
	    from: 20,
	    step: 10,
	    prettify_enabled: true
	});	

	$('#rangeThird').ionRangeSlider({
	    grid: true,
	    min: 6,
	    max: 52,
	    from: 6,
	    step: 2,
	    prettify_enabled: true
	});

	var slider3 = $("#rangeThird").data("ionRangeSlider");	

	$('#21520,#21515,#22115').on('change',function () {
		slider3.update({
			min: 8,
			from: 8,
		    max: 54,	    	
	    	step: 4
		});	
		start[2] = 380;		
	});

	$('#22030,#22020').on('change',function () {
		slider3.update({
			min: 6,
			from: 6,
		    max: 52,	    	
	    	step: 2
		});
		start[2] = 460;		
	});

	$('#rangeFour').ionRangeSlider({
	    grid: true,
	    min: 6,
	    max: 52,
	    from: 6,
	    step: 2,
	    prettify_enabled: true
	});	

	var slider4 = $("#rangeFour").data("ionRangeSlider");

	$('#31520,#31515,#32115').on('change',function () {
		slider4.update({
		    max: 54,	    	
	    	step: 4
		});	
		start[3] = 630;		
	});

	$('#32030,#32020').on('change',function () {
		slider4.update({
		    max: 52,	    	
	    	step: 2
		});
		start[3] = 660;		
	});

	function changePrice(range,radio){
		var inputValue;
		var radioValue;
		var result;		
		$('.horizontal li a').on('click',function(){
			setTimeout(function() {
				calc();				
			}, 300);
		});
		function calc() {			
			inputValue = $('#range'+ range +'').val();	
			if(range === 'Second'){
				switch(inputValue){
					case '20':
						inputValue = 1395;
						break;
					case '30':
						inputValue = 1950;
						break;
					case '40':
						inputValue = 2100;
						break;
					default:
						inputValue = 2300;
						break;
				}
			}
			radioValue = $('input[name="size'+ radio +'"]:checked').val();			
			result = inputValue * radioValue + start[radio-1];
			$('#price'+ radio +'').text(result + ' сом');			
		}
		calc();
		$('#range'+ range +',input[name="size'+ radio +'"]').on('change',function(){
			calc();
		});		
	}

	changePrice('First',1);
	changePrice('Second',2);
	changePrice('Third',3);
	changePrice('Four',4);
	
	$('.tabslet').tabslet();	

	$(document).on('click','.click-slide',function(){
		var img = $(this).find('.goToUp').find('img').attr('src');
		var descr = $(this).find('.goToUp').find('p').html();
		var title = $(this).find('.goToUp').find('h4').html();
		var price = $(this).find('.goToUp').find('span').html();
		$('#image').attr('src',img);
		$('#description').html(descr);
		$('#title').html(title);
		$('#price').html(price);
	});

	$('.fancybox').fancybox();


	$(".f_contact").submit(function(e){
	    e.preventDefault();
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: $(this).serialize()
        }).done(function() {
            $.fancybox( $('#modal-thanks'), {
                padding: 0,
                fitToView: false
            } ); 
            setTimeout(function() {
                $.fancybox.close();
            }, 3000);
        });
         return false; 
     });

	$(function(){
		if ($(window).scrollTop()>="600") $("#ToTop").fadeIn("slow")
		$(window).scroll(function(){
		  if ($(window).scrollTop()<="600") $("#ToTop").fadeOut("slow")
		  else $("#ToTop").fadeIn("slow")
		});
		if ($(window).scrollTop()<=$(document).height()-"999") $("#OnBottom").fadeIn("slow")
		$(window).scroll(function(){
		  if ($(window).scrollTop()>=$(document).height()-"999") $("#OnBottom").fadeOut("slow")
		  else $("#OnBottom").fadeIn("slow")
		});
		$("#ToTop").click(function(){$("html,body").animate({scrollTop:0},"slow")})
		$("#OnBottom").click(function(){$("html,body").animate({scrollTop:$(document).height()},"slow")})
	});

});



